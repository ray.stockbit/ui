import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import typescript from '@rollup/plugin-typescript';

import del from 'rollup-plugin-delete';
import dts from 'rollup-plugin-dts';
import postcss from 'rollup-plugin-postcss';
import { terser } from 'rollup-plugin-terser';
import peerDepsExternal from 'rollup-plugin-peer-deps-external';

import postcssImport from 'postcss-import';
import env from 'postcss-preset-env';
import autoprefixer from 'autoprefixer';

const packageJson = require('./package.json');

export default [
  {
    input: 'src/index.ts',
    output: [
      {
        file: packageJson.main,
        format: 'cjs',
        sourcemap: true,
      },
      {
        file: packageJson.module,
        format: 'esm',
        sourcemap: true,
      },
    ],
    plugins: [
      typescript({
        exclude: [
          // Exclude test files
          /\.test.((js|jsx|ts|tsx))$/,
          // Exclude story files
          /\.stories.((js|jsx|ts|tsx|mdx))$/,
        ],
        tsconfig: './tsconfig.json',
      }),
      resolve({
        extensions: ['.mjs', '.js', '.json', '.node', '.css'],
      }),
      commonjs(),
      postcss({
        modules: {
          exportLocalsConvention: 'camelCaseOnly',
        },
        minimize: true,
        extract: 'main.css',
        plugins: [env(), autoprefixer(), postcssImport()],
      }),
      peerDepsExternal(),
      terser(),
    ],
    external: ['react', 'react-dom'],
  },
  {
    input: 'dist/index.d.ts',
    output: [{ file: 'dist/index.d.ts', format: 'esm' }],
    plugins: [
      dts(),
      del({
        hook: 'buildEnd',
        targets: ['./dist/atoms', './dist/molecules', './dist/organisms'],
      }),
    ],
    external: [/\.css$/, 'react', 'react-dom'],
  },
];
