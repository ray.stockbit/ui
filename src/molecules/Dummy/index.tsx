import React from 'react'

const Dummy = () => {
  return <h1>Only dummy component</h1>;
};

export default Dummy;