import React, { CSSProperties, ReactNode } from 'react';
import styles from './Text.module.css';
import clsx from 'clsx';

export interface TextProps {
  children: ReactNode;
  className?: string;
  style?: CSSProperties;
}

const Text = ({ className, children, ...props }: TextProps) => {
  return (
    <p className={clsx(styles.root, className)} {...props}>
      {children}
    </p>
  );
};

export default Text;
