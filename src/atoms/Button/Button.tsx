import React, { CSSProperties } from 'react';
import styles from './Button.module.css';
import clsx from 'clsx';

export interface ButtonProps {
  className?: string;
  style?: CSSProperties;
  label: string;
  disabled?: boolean;
  variant?: 'primary' | 'secondary';
}

const Button = ({
  label,
  className,
  disabled = false,
  variant = 'primary',
  ...props
}: ButtonProps) => {
  return (
    <button
      className={clsx(
        styles.root,
        styles[variant],
        disabled && styles.disabled,
        className,
      )}
      disabled={disabled}
      {...props}
    >
      {label}
    </button>
  );
};

export default Button;
